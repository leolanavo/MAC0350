--------------------------------------------------------------------------------
--- Insere trilhas
--------------------------------------------------------------------------------

INSERT INTO
public.trilhas(
  codigo_trilha,
  nome,
  descricao,
  minimo_disciplinas,
  minimo_modulos)
VALUES
( 'IA', 'Inteligencia Artificial', 'robozinhos', 10, 2 ),
( 'DS', 'Ciência de Dados', 'hype', 8, 2);

--------------------------------------------------------------------------------
--- Insere modulos
--------------------------------------------------------------------------------

INSERT INTO
public.modulos(
  codigo_modulo,
  minimo_disciplinas,
  id_trilha)
VALUES
( 'Teoria', 10, (SELECT id_trilha FROM public.trilhas WHERE codigo_trilha='IA') ),
( 'Sistemas', 8, (SELECT id_trilha FROM public.trilhas WHERE codigo_trilha='IA') );

--------------------------------------------------------------------------------
--- Insere disciplinas
--------------------------------------------------------------------------------

INSERT INTO
public.disciplinas(
  codigo_disciplina,
  creditos_aula,
  creditos_trabalho,
  carga_horaria,
  ativacao,
  nome)
VALUES
( 'MAC0101', 4, 0, 60, '2015-01-01', 'Palestras I'),
( 'MAC0110', 4, 0, 60, '1998-01-01', 'Introdução a Computação' ),
( 'MAC0121', 4, 0, 60, '2015-01-01', 'ED I'),
( 'MAC0350', 4, 2, 60, '2017-06-01', 'Sisteminhas'),
( 'MAC0426', 4, 4, 60, '1998-01-01', 'SisBD'),
( 'MAC0444', 4, 0, 60, '1998-02-02', 'Análise'),
( 'MAC0414', 4, 0, 60, '1998-03-03', 'SO'),
( 'MAE0399', 4, 3, 60, '1998-04-04', 'Estocásticos');

--------------------------------------------------------------------------------
--- Insere disciplinas que compoem modulos
--------------------------------------------------------------------------------

INSERT INTO 
public.grade_obrigatoria(
  id_disciplina, 
  ano_grade_obrigatoria)
VALUES
(
  (SELECT id_disciplina FROM public.disciplinas WHERE codigo_disciplina = 'MAC0110' LIMIT 1),
  '1998-01-01'
),
(
  (SELECT id_disciplina FROM public.disciplinas WHERE codigo_disciplina = 'MAC0121' LIMIT 1),
  '2015-01-01'
),
(
  (SELECT id_disciplina FROM public.disciplinas WHERE codigo_disciplina = 'MAC0350' LIMIT 1),
  '2017-06-01'
);

INSERT INTO
public.grade_optativa(
  id_disciplina,
  ano_grade_optativa,
  eletiva)
VALUES
(
  (SELECT id_disciplina FROM public.disciplinas WHERE codigo_disciplina = 'MAC0426' LIMIT 1),
  '1998-01-01',
  true
),
(
  (SELECT id_disciplina FROM public.disciplinas WHERE codigo_disciplina = 'MAC0414' LIMIT 1),
  '1998-03-03',
  true
),
(
  (SELECT id_disciplina FROM public.disciplinas WHERE codigo_disciplina = 'MAE0399' LIMIT 1),
  '1998-04-04',
  false
);

INSERT INTO
public.optativas_compoem_modulos(
  id_disciplina,
  ano_grade_optativa,
  id_modulo)
VALUES
(
  (SELECT id_disciplina FROM public.disciplinas WHERE codigo_disciplina = 'MAC0426' LIMIT 1),
  '1998-01-01',
  (SELECT id_modulo FROM public.modulos WHERE codigo_modulo = 'Sistemas' LIMIT 1)
),  
(
  (SELECT id_disciplina FROM public.disciplinas WHERE codigo_disciplina = 'MAE0399' LIMIT 1),
  '1998-04-04',
  (SELECT id_modulo FROM public.modulos WHERE codigo_modulo = 'Teoria' LIMIT 1)
);

--------------------------------------------------------------------------------
--- Insere pessoas em seus respectivos cargos
--------------------------------------------------------------------------------

INSERT INTO
public.pessoas(nome)
VALUES
( 'Jef'    ),
( 'Décio'  ),
( 'Renato' );

INSERT INTO
public.administradores(id_pessoa, nusp_administrador, data_inicio)
VALUES
(
  (SELECT id_pessoa FROM public.pessoas WHERE nome = 'Jef' LIMIT 1),
  '0000001',
  '2018-01-01'
);

INSERT INTO
public.professores(id_pessoa, nusp_professor, departamento)
VALUES
(
  (SELECT id_pessoa FROM public.pessoas WHERE nome = 'Jef' LIMIT 1),
  '0000001',
  'Departamento de Ciência da Computação'
);

INSERT INTO
public.alunos(id_pessoa, nusp_aluno, turma_ingresso)
VALUES
(
  (SELECT id_pessoa FROM public.pessoas WHERE nome = 'Décio' LIMIT 1),
  '0000002',
  '2015-03-01'
),
(
  (SELECT id_pessoa FROM public.pessoas WHERE nome = 'Renato' LIMIT 1),
  '0000003',
  '2012-03-01'
);

--------------------------------------------------------------------------------
--- Insere as discplinas que são pré-requesitos
--------------------------------------------------------------------------------
INSERT INTO 
public.prerequisitos (id_disciplina, id_prerequisito, total)
VALUES 
(
  (SELECT id_disciplina FROM public.disciplinas WHERE codigo_disciplina='MAC0350'),
  (SELECT id_disciplina FROM public.disciplinas WHERE codigo_disciplina='MAC0121'),
  true
),
(
  (SELECT id_disciplina FROM public.disciplinas WHERE codigo_disciplina='MAC0121'),
  (SELECT id_disciplina FROM public.disciplinas WHERE codigo_disciplina='MAC0110'),
  true
);

--------------------------------------------------------------------------------
--- Insere as discplinas que os professores oferecem
--------------------------------------------------------------------------------
INSERT INTO
public.professores_oferecem_disciplinas(id_pessoa, nusp_professor, id_disciplina, ano_semestre)
VALUES
(
  (SELECT id_pessoa FROM public.pessoas WHERE nome='Jef' LIMIT 1),
  '0000001',
  (SELECT id_disciplina FROM public.disciplinas WHERE codigo_disciplina='MAC0110'),
  2012.1
),
(
  (SELECT id_pessoa FROM public.pessoas WHERE nome='Jef' LIMIT 1),
  '0000001',
  (SELECT id_disciplina FROM public.disciplinas WHERE codigo_disciplina='MAC0350'),
  2018.1
),
(
  (SELECT id_pessoa FROM public.pessoas WHERE nome='Jef' LIMIT 1),
  '0000001',
  (SELECT id_disciplina FROM public.disciplinas WHERE codigo_disciplina='MAC0426'),
  2018.1
),
(
  (SELECT id_pessoa FROM public.pessoas WHERE nome='Jef' LIMIT 1),
  '0000001',
  (SELECT id_disciplina FROM public.disciplinas WHERE codigo_disciplina='MAE0399'),
  2018.1
);

--------------------------------------------------------------------------------
--- Insere as discplinas que os alunos cursam
--------------------------------------------------------------------------------

INSERT INTO
public.alunos_cursam_disciplinas(id_pessoa, nusp_aluno, id_oferecimento, nota)
VALUES
(
  (SELECT id_pessoa FROM public.alunos WHERE nusp_aluno = '0000003' LIMIT 1), 
  '0000003', 
  (SELECT id_oferecimento FROM public.professores_oferecem_disciplinas 
  WHERE 
    id_pessoa=(SELECT id_pessoa FROM public.pessoas WHERE nome='Jef' LIMIT 1) AND
    nusp_professor='0000001' AND
    id_disciplina=(SELECT id_disciplina FROM public.disciplinas WHERE codigo_disciplina='MAC0110') AND
    ano_semestre=2012.1), 
  5
),
(
  (SELECT id_pessoa FROM public.alunos WHERE nusp_aluno = '0000003' LIMIT 1), 
  '0000003', 
  (SELECT id_oferecimento FROM public.professores_oferecem_disciplinas 
  WHERE 
    id_pessoa=(SELECT id_pessoa FROM public.pessoas WHERE nome='Jef' LIMIT 1) AND
    nusp_professor='0000001' AND
    id_disciplina=(SELECT id_disciplina FROM public.disciplinas WHERE codigo_disciplina='MAC0426') AND
    ano_semestre=2018.1), 
  5
),
(
  (SELECT id_pessoa FROM public.alunos WHERE nusp_aluno = '0000003' LIMIT 1), 
  '0000003', 
  (SELECT id_oferecimento FROM public.professores_oferecem_disciplinas 
  WHERE 
    id_pessoa=(SELECT id_pessoa FROM public.pessoas WHERE nome='Jef' LIMIT 1) AND
    nusp_professor='0000001' AND
    id_disciplina=(SELECT id_disciplina FROM public.disciplinas WHERE codigo_disciplina='MAE0399') AND
    ano_semestre=2018.1), 
  5
),
(
  (SELECT id_pessoa FROM public.alunos WHERE nusp_aluno = '0000002' LIMIT 1), 
  '0000002', 
  (SELECT id_oferecimento FROM public.professores_oferecem_disciplinas 
  WHERE 
    id_pessoa=(SELECT id_pessoa FROM public.pessoas WHERE nome='Jef' LIMIT 1) AND
    nusp_professor='0000001' AND
    id_disciplina=(SELECT id_disciplina FROM public.disciplinas WHERE codigo_disciplina='MAC0110') AND
    ano_semestre=2012.1), 
  2.9
);

--------------------------------------------------------------------------------
--- Insere planos
--------------------------------------------------------------------------------
INSERT INTO
public.alunos_planejam_disciplinas(
  num_plano, 
  id_pessoa, 
  nusp_aluno, 
  id_disciplina, 
  ano_semestre)

VALUES
( 
  1,
  (SELECT id_pessoa FROM public.alunos WHERE nusp_aluno = '0000003' LIMIT 1),
  '0000003',
  (SELECT id_disciplina FROM public.disciplinas WHERE codigo_disciplina = 'MAC0121' LIMIT 1),
  '2012.2'
);
--------------------------------------------------------------------------------
--- Cria estrutura de autenticação
--------------------------------------------------------------------------------

INSERT INTO
auth.usuarios(email_usuario, senha, expira)
VALUES
( 'jef@ime.usp.br', '12345', '2019-01-01' ),
( 'decio@ime.usp.br', '12345', '2019-01-01' ),
( 'renatocf@ime.usp.br', '12345', '2019-01-01' );

INSERT INTO
public.pessoas_geram_usuarios(id_pessoa, id_usuario)
VALUES
(
  (SELECT id_pessoa FROM public.pessoas WHERE nome = 'Jef' LIMIT 1),
  (SELECT id_usuario FROM auth.usuarios WHERE email_usuario = 'jef@ime.usp.br')
),
(
  (SELECT id_pessoa FROM public.pessoas WHERE nome = 'Décio' LIMIT 1),
  (SELECT id_usuario FROM auth.usuarios WHERE email_usuario = 'decio@ime.usp.br')
),
(
  (SELECT id_pessoa FROM public.pessoas WHERE nome = 'Renato' LIMIT 1),
  (SELECT id_usuario FROM auth.usuarios WHERE email_usuario = 'renatocf@ime.usp.br')
);

INSERT INTO
auth.perfis(nome_perfil)
VALUES
( 'administrador' ), -- 1
( 'professor' ), -- 2
( 'aluno' ); -- 3

INSERT INTO
auth.servicos(caminho_servico)
VALUES
( '/'             ), -- 1
( '/index'        ), -- 2
( '/estrutura'    ), -- 3
( '/plano'        ), -- 4
( '/trilhas'        ), -- 5
( '/trilhas/index'  ), -- 6
( '/trilhas/new'    ), -- 7
( '/trilhas/edit'   ), -- 8
( '/trilhas/show'   ); -- 9

INSERT INTO
auth.usuarios_possuem_perfis(id_usuario, id_perfil, expira)
VALUES
(
  (SELECT id_usuario FROM auth.usuarios WHERE email_usuario = 'jef@ime.usp.br'),
  (SELECT id_perfil FROM auth.perfis WHERE nome_perfil = 'administrador'),
  '2019-01-01'
),
(
  (SELECT id_usuario FROM auth.usuarios WHERE email_usuario = 'jef@ime.usp.br'),
  (SELECT id_perfil FROM auth.perfis WHERE nome_perfil = 'professor'),
  '2019-01-01'
),
(
  (SELECT id_usuario FROM auth.usuarios WHERE email_usuario = 'decio@ime.usp.br'),
  (SELECT id_perfil FROM auth.perfis WHERE nome_perfil = 'aluno'),
  '2019-01-01'
),
(
  (SELECT id_usuario FROM auth.usuarios WHERE email_usuario = 'renatocf@ime.usp.br'),
  (SELECT id_perfil FROM auth.perfis WHERE nome_perfil = 'aluno'),
  '2019-01-01'
);

INSERT INTO
auth.perfis_acessam_servicos(id_perfil, id_servico, expira)
VALUES
-- Administrador
(
  (SELECT id_perfil FROM auth.perfis WHERE nome_perfil = 'administrador'),
  (SELECT id_servico FROM auth.servicos WHERE caminho_servico = '/'),
  '2019-01-01'
),
(
  (SELECT id_perfil FROM auth.perfis WHERE nome_perfil = 'administrador'),
  (SELECT id_servico FROM auth.servicos WHERE caminho_servico = '/index'),
  '2019-01-01'
),
(
  (SELECT id_perfil FROM auth.perfis WHERE nome_perfil = 'administrador'),
  (SELECT id_servico FROM auth.servicos WHERE caminho_servico = '/estrutura'),
  '2019-01-01'
),
(
  (SELECT id_perfil FROM auth.perfis WHERE nome_perfil = 'administrador'),
  (SELECT id_servico FROM auth.servicos WHERE caminho_servico = '/plano'),
  '2019-01-01'
),
(
  (SELECT id_perfil FROM auth.perfis WHERE nome_perfil = 'administrador'),
  (SELECT id_servico FROM auth.servicos WHERE caminho_servico = '/cruds'),
  '2019-01-01'
),
(
  (SELECT id_perfil FROM auth.perfis WHERE nome_perfil = 'administrador'),
  (SELECT id_servico FROM auth.servicos WHERE caminho_servico = '/cruds/index'),
  '2019-01-01'
),
(
  (SELECT id_perfil FROM auth.perfis WHERE nome_perfil = 'administrador'),
  (SELECT id_servico FROM auth.servicos WHERE caminho_servico = '/cruds/new'),
  '2019-01-01'
),
(
  (SELECT id_perfil FROM auth.perfis WHERE nome_perfil = 'administrador'),
  (SELECT id_servico FROM auth.servicos WHERE caminho_servico = '/cruds/edit'),
  '2019-01-01'
),
(
  (SELECT id_perfil FROM auth.perfis WHERE nome_perfil = 'administrador'),
  (SELECT id_servico FROM auth.servicos WHERE caminho_servico = '/cruds/show'),
  '2019-01-01'
),
-- Professor
(
  (SELECT id_perfil FROM auth.perfis WHERE nome_perfil = 'professor'),
  (SELECT id_servico FROM auth.servicos WHERE caminho_servico = '/'),
  '2019-01-01'
),
(
  (SELECT id_perfil FROM auth.perfis WHERE nome_perfil = 'professor'),
  (SELECT id_servico FROM auth.servicos WHERE caminho_servico = '/index'),
  '2019-01-01'
),
(
  (SELECT id_perfil FROM auth.perfis WHERE nome_perfil = 'professor'),
  (SELECT id_servico FROM auth.servicos WHERE caminho_servico = '/estrutura'),
  '2019-01-01'
),
(
  (SELECT id_perfil FROM auth.perfis WHERE nome_perfil = 'professor'),
  (SELECT id_servico FROM auth.servicos WHERE caminho_servico = '/plano'),
  '2019-01-01'
),
-- Estudante
(
  (SELECT id_perfil FROM auth.perfis WHERE nome_perfil = 'aluno'),
  (SELECT id_servico FROM auth.servicos WHERE caminho_servico = '/'),
  '2019-01-01'
),
(
  (SELECT id_perfil FROM auth.perfis WHERE nome_perfil = 'aluno'),
  (SELECT id_servico FROM auth.servicos WHERE caminho_servico = '/index'),
  '2019-01-01'
),
(
  (SELECT id_perfil FROM auth.perfis WHERE nome_perfil = 'aluno'),
  (SELECT id_servico FROM auth.servicos WHERE caminho_servico = '/estrutura'),
  '2019-01-01'
),
(
  (SELECT id_perfil FROM auth.perfis WHERE nome_perfil = 'aluno'),
  (SELECT id_servico FROM auth.servicos WHERE caminho_servico = '/plano'),
  '2019-01-01'
);
