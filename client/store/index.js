import Vuex from 'vuex';
import VuexPersistence from 'vuex-persist';
import axios from 'axios';

const baseAPIUrl = 'http://localhost:3000';
const statuses = ["nulo", "cursada", "planejada"];
const symbols = ["", "&#9989;", "&#128393;"];
const colors = ["#C0C0C0", "#3F51B5", "#FF9800"];

const vuexLocal = new VuexPersistence({
  storage: window.localStorage,
});

const decodeJWT = token => {
  if (token == '') return;

  let base64Url = token.split('.')[1];
  let base64 = base64Url.replace('-', '+').replace('_', '/');
  return JSON.parse(window.atob(base64));
};

export default () => {
  return new Vuex.Store({
    plugins: [vuexLocal.plugin],

    state: {
      email: '',
      token: '',
      nusp: '',
      nome: '',
      startingYear: '',
      cred_obg_approved: 0,
      cred_el_approved: 0,
      cred_livre_approved: 0,
      cred_obg_planned: 0,
      cred_el_planned: 0,
      cred_livre_planned: 0,
      view: 'curso',
      onIndex: true,
      activeSubject: '',
      selectedSemester: 'Geral',
      approvedSubjects: [],
      plannedSubjects: [],
      blockedSubjects: [],
      approvedSemester: [],
      plannedSemester: [],
      planNumber: '',
      status: {},
      planning: {},
      numPlanNext: 0,
      planNumbers: [],
      activeTrilha: {},
      activeModulo: {},
      activeOptativa: {},
      mode: '',
    },

    getters: {
      getStatus(state) {
        return code => { return state.status[`${code}`] }
      },

      isApproved(state) {
        return code => { return state.approvedSubjects.includes(code) };
      },

      isPlanned(state) {
        return code => { return state.plannedSubjects.includes(code) };
      },

      getMode(state) { return state.mode },

      getActiveTrilha(state) { return state.activeTrilha },

      getActiveModulo(state) { return state.activeModulo },

      getActiveOptativa(state) { return state.activeOptativa },

      getPlanNumbers(state) { return state.planNumbers },

      getNumPlanNext(state) { return state.numPlanNext },

      getPlanning(state) { return state.planning },

      isOnCourse(state) { return state.view === 'curso'; },

      isOnIndex(state) { return state.onIndex; },

      getSelectedSemester(state) { return state.selectedSemester },

      getPlanNumber(state) { return state.planNumber },

      getApprovedSubjects(state) { return state.approvedSubjects },

      getPlannedSubjects(state) { return state.plannedSubjects },

      getBlockedSubjects(state) { return state.blockedSubjects },

      getApprovedSemester(state) { return state.approvedSemester },

      getPlannedSemester(state) { return state.plannedSemester },

      isAuthenticated(state) { return state.token !== ''; },

      getToken(state) { return state.token; },

      getEmail(state) { return state.email; },

      getNusp(state) { return state.nusp; },

      getStartingYear(state) { return state.startingYear; },

      getNome(state) { return state.nome; },

      getActiveSubject(state) { return state.activeSubject; },

      getCredObgApproved(state) { return state.cred_obg_approved; },

      getCredElApproved(state) { return state.cred_el_approved; },

      getCredLivreApproved(state) { return state.cred_livre_approved; },

      getCredObgPlanned(state) { return state.cred_obg_planned; },

      getCredElPlanned(state) { return state.cred_el_planned; },

      getCredLivrePlanned(state) { return state.cred_livre_planned; },

      getUUID(state) {
        const decodedToken = decodeJWT(state.token);
        if (decodedToken === undefined) return '';
        return decodeJWT(state.token).id_usuario;
      },

      getSemesterNumber(state) {
        let sem = state.selectedSemester.slice(0, -1);
        let semNumber = +state.startingYear + Math.floor(+sem/2);
        return +sem % 2 === 0 ? semNumber + '.2' : semNumber + '.1' ;
      }
    },

    actions: {
      async getNusp({ commit, getters }) {
        const uuid = getters.getUUID;
        const response = await axios.post(`${baseAPIUrl}/rpc/busca_aluno_nusp`, {
          "id_usr": uuid
        });
        commit('setNusp', { nusp: response.data });
      },

      async getNome ({ commit, getters }) {
        const uuid = getters.getUUID;
        const response = await axios.post(`${baseAPIUrl}/rpc/busca_nome_usuario`, {
          "id_usr": uuid
        });
        commit('setNome', { nome: response.data });
      },

      async getStartingYear({ commit, getters }) {
        const nusp = getters.getNusp;
        const response = await axios.get(`${baseAPIUrl}/alunos?nusp_aluno=eq.${nusp}`);
        const startingYear = response.data[0].turma_ingresso.split('-')[0];
        commit('setStartingYear', { startingYear });
      },

      async setSemester({commit}, { type, name , payload, setter }) {
        const response = 
          await this.$axios.$post(`${baseAPIUrl}/rpc/disciplinas_${type}_semestre`, payload);

        let obj = {};
        obj[`${name}Semester`] = response;

        commit(`set${setter}Semester`, obj);
      },

      async getSubjects({ commit }, {payload, url, setter, name}) {
        const response = await axios.post(`${baseAPIUrl}/rpc/${url}`, payload)

        if (response === undefined) return;

        const subjects = response.data.map(obj => {
          return obj['codigo_disciplina']
        })

        let obj = {};
        obj[`${name}`] = subjects;

        commit(`${setter}`, obj);
      },

      setSubjectStatus({ commit, getters }, { code }) {
        const status = getters.getStatus(code).status;
        const index = (statuses.indexOf(status) + 1) % statuses.length;

        commit('setStatus', {
          code,
          index
        })
      },

      async getPlans({ commit, getters }) {
        const nusp = getters.getNusp;
        const response = await axios.post(`${baseAPIUrl}/rpc/busca_planos`, { nusp });

        const planNumbers = response.data.map(num => {
          return num.num_plano;
        });

        let max = 0;
        planNumbers.map(num => {
          if (num > max) max = num;
        })
        console.log(max);

        const numPlanNext = max + 1; 

        commit('setPlanNumbers', { planNumbers });
        commit('setNumPlanNext', { numPlanNext });
      },

      async getActivePlan({commit, getters}) {
        const response = await axios.post(`${baseAPIUrl}/rpc/busca_plano_numero`, {
          nusp: getters.getNusp,
          n_plano: getters.getPlanNumber
        });

        console.log(response);
      },

      addToPlanning({ commit, getters }, {code, counter}) {
        let semNumber = +getters.getStartingYear + Math.ceil(counter/2) - 1;
        semNumber = counter % 2 === 0 ? semNumber + '.2' : semNumber + '.1' ;
        
        commit('setPlanning', { code, semNumber });
      },

      async commitPlan({ commit, getters, dispatch }) {
        const nusp = getters.getNusp;
        const planning = getters.getPlanning;
        let numPlanNext = getters.getNumPlanNext;

        for (let i in planning) {
          await axios.post(`${baseAPIUrl}/rpc/cria_plano`, {
            nusp,
            cdg_disciplina: i,
            ano_semestre: +planning[i].semNumber,
            num_plano: numPlanNext,
          },
          { headers: {Authorization: `Bearer ${getters.getToken}`} }
          );
        }

        numPlanNext++;
        commit('setNumPlanNext', { numPlanNext });
        commit('cleanPlanning');
      },
    },

    mutations: {
      setEmail(state, { email }) { state.email = email; },

      setToken(state, { token }) { state.token = token; },

      logOut(state) {
        state.email = '';
        state.token = '';
        state.nusp = '';
        state.nome = '';
        state.startingYear = '';
        state.cred_obg_approved = 0;
        state.cred_el_approved = 0;
        state.cred_livre_approved = 0;
        state.cred_obg_planned = 0;
        state.cred_el_planned = 0;
        state.cred_livre_planned = 0;
        state.view = 'curso';
        state.onIndex = true;
        state.activeSubject = '';
        state.selectedSemester = 'Geral';
        state.approvedSubjects = [];
        state.plannedSubjects = [];
        state.blockedSubjects = [];
        state.approvedSemester = [];
        state.plannedSemester = [];
        state.planning = {};
        state.numPlanNext = 0;
        state.planNumber = '';
        state.planNumbers = [];
        state.activeTrilha = {};
        state.activeModulo = {};
        state.activeOptativa = {};
        state.mode = '';

        let newState = Object.assign({}, state.status);

        for (let key in newState) {
          newState[key] = {
            status: statuses[0],
            symbol: symbols[0],
            color: colors[0]
          }
        }

        state.status = newState;
      },

      setNusp(state, { nusp }) { state.nusp = nusp; },

      setStartingYear(state, { startingYear }) { state.startingYear = startingYear; },

      setNome(state, { nome }) { state.nome = nome; },

      setView(state, { view }) { state.view = view; },

      setOnIndex(state, {onIndex}) { state.onIndex = onIndex; },

      setPlanNumber(state, {planNumber}) { state.planNumber = planNumber },

      setSelectedSemester(state, { selectedSemester }) {
        state.selectedSemester = selectedSemester;
      },

      setApprovedSubjects(state, { approvedSubjects }) {
        state.approvedSubjects = approvedSubjects;
      },

      setPlannedSubjects(state, { plannedSubjects }) {
        state.plannedSubjects = plannedSubjects;
      },

      setBlockedSubjects(state, { blockedSubjects }) {
        state.blockedSubjects = blockedSubjects;
      },

      setApprovedSemester(state, { approvedSemester }) {
        state.approvedSemester = approvedSemester;
      },

      setPlannedSemester(state, { plannedSemester }) {
        state.plannedSemester = plannedSemester;
      },

      setActiveSubject(state, {code}) { state.activeSubject = code; },

      setCredits(state, 
        { cred_obg, cred_el, cred_livre }) {
        state[`cred_obg_approved`] = cred_obg;
        state[`cred_el_approved`] = cred_el;
        state[`cred_livre_approved`] = cred_livre;
      },

      addCredits(state, {credits, code, type, name}) {
        state[`cred_${type}_${name}`] += credits;
        state[`${name}Subjects`].push(code);
      },

      removeCredits(state, {credits, code, type, name}) {
        state[`cred_${type}_${name}`] -= credits;
        state[`${name}Subjects`].splice(state[`${name}Subjects`].indexOf(code), 1);
      },

      setStatus(state, { code, index }) {
        let newState = { ...state.status };
        newState[`${code}`] =  { 
          status: statuses[index],
          symbol: symbols[index],
          color: colors[index]
        }

        state.status = newState;
      },

      setPlannedAsNull(state) {
        let newState = Object.assign({}, state.status);

        for (let key in newState) {
          if (newState[key].status === "planejada") {
            newState[key] = {
              status: statuses[0],
              symbol: symbols[0],
              color: colors[0]
            }
          }
        }

        state.status = newState;

        state.cred_obg_planned = 0;
        state.cred_el_planned = 0;
        state.cred_livre_planned = 0;
      },

      setPlanning(state, { code, semNumber }) {
        state.planning[`${code}`] = { semNumber }
      },

      setPlanNumbers(state, { planNumbers }) {
        state.planNumbers = planNumbers;
      },

      setNumPlanNext(state, { numPlanNext }) {
        state.numPlanNext = numPlanNext;
      },

      cleanPlanning(state) {
        state.planning = {};
      },

      setActiveTrilha(state, { trilha }) {
        state.activeTrilha = trilha;
      },

      setActiveModulo(state, { modulo }) {
        state.activeModulo = modulo;
      },

      setActiveOptativa(state, { optativa }) {
        state.activeOptativa = optativa;
      },

      setMode(state, { mode }) {
        state.mode = mode;
      }
    }
  });
}
